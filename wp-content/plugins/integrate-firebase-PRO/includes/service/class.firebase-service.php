<?php
/**
 * Firebase Service
 */

defined('ABSPATH') || exit;

if (!class_exists('FirebaseService', false)):

    /**
     * FirebaseService Class
     */
    class FirebaseService {
        protected $firebase_settings = null;
        protected $options_wordpress = null;

        public function __construct() {
            $this->firebase_settings = get_option('firebase_settings');
            $this->options_wordpress = get_option("firebase_wordpress");
        }

        public function get_firebase_setting() {
            return $this->firebase_settings;
        }

        private function collection_name_generetor($post_type) {
            $type_object = get_post_type_object($post_type);

            $plural_name = $type_object->labels->name;
            $name_array = preg_split("/[_,\- ]+/", $plural_name);
            $name_array = array_map('ucfirst', $name_array);

            return 'wp' . implode("", $name_array);
        }

        /**
         * Send Data to Firebase Database
         *
         * @param [type] $database_type realtime | firestore
         * @param [type] $collection_name
         * @param [type] $doc_id
         * @param [type] $data
         * @return void
         */
        public function send_data_to_firebase($database_type, $collection_name, $doc_id, $data) {
            $base_domain = $this->firebase_settings['base_domain'];
            $url = $base_domain . "/api-database/v1/createDoc";
            $api_token = $this->firebase_settings['dashboard_api_token'];

            // Prepare data
            $prepared_data = new stdClass();
            $prepared_data->dbType = $database_type;
            $prepared_data->collection = $collection_name;
            $prepared_data->docId = (string) $doc_id;
            $prepared_data->data = $data;

            $response = wp_remote_post($url, array(
                'method' => 'POST',
                'headers' => array(
                    'api-token' => $api_token,
                    'source' => 'dashboard',
                    'Content-Type' => 'application/json; charset=utf-8',
                ),
                'body' => json_encode($prepared_data),
            ));

            if (!is_wp_error($response)) {
                $result = json_decode($response['body']);
                if ($result->status) {
                    return $result;
                } else {
                    error_log($result->message);
                    return false;
                }
            }
        }

        public function save_wordpress_data_to_firebase($post_id, $post) {
            if (
            is_array($this->options_wordpress['wp_sync_post_types'])
            || isset($this->options_wordpress['wp_sync_custom_post_types'])
        ) {
                $collection_name = null;
                $post_type = $post->post_type;

                if (
                in_array($post_type, $this->options_wordpress['wp_sync_post_types'])
                || strpos($this->options_wordpress['wp_sync_custom_post_types'], $post_type) !== false
            ) {
                    $collection_name = $this->collection_name_generetor($post_type);
                }
                if ($collection_name) {
                    $database_type = $this->options_wordpress['wp_sync_database_type'];
                    $doc_id = (string) $post_id;
                    apply_filters('firebase_save_data_to_database', $database_type, $collection_name, $doc_id, $post);

                } else {
                    error_log('Integrate Firebase PRO does not support post type: ' . $post_type);
                }
            }
        }

        public function sync_user_to_firebase($firebase_user) {
            if (!empty($this->options_wordpress['wp_sync_users'])) {

                unset($firebase_user['password']);
                $filtered_data = array_filter($firebase_user);
                $database_type = $this->options_wordpress['wp_sync_database_type'];
                $collection_name = $this->options_wordpress['wp_sync_users'];
                $doc_id = (string) $filtered_data['userId'];

                apply_filters('firebase_save_data_to_database', $database_type, $collection_name, $doc_id, $filtered_data);
            }
        }
    }

endif;

return new FirebaseService();
