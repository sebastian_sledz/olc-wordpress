<?php

/**
 * Firebase WordPress Filters Hooks
 */

class FirebaseWordPressFilters {
    private static $initiated = false;
    private static $firebase_service;
    private static $firebase_settings;

    public static function init() {
        if (!self::$initiated) {
            self::init_hooks();
        }
    }

    public static function init_hooks() {
        self::$initiated = true;
        self::$firebase_settings = get_option('firebase_settings');

        if (
            self::$firebase_settings &&
            isset(self::$firebase_settings['base_domain']) &&
            isset(self::$firebase_settings['base_domain'])) {
            self::$firebase_service = include FIREBASE_WP__PLUGIN_DIR . 'includes/service/class.firebase-service.php';

            // Custom filters
            add_filter('firebase_save_data_to_database', array('FirebaseWordPressFilters', 'save_data_to_database'), 10, 4);
        }

    }

    public static function save_data_to_database($database_type, $collection_name, $doc_id, $data) {
        return self::$firebase_service->send_data_to_firebase($database_type, $collection_name, $doc_id, $data);
    }
}
