<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $current_user;
$current_user_role = $current_user->roles;

$function_obj				 = Ld_Dashboard_Functions::instance();
$ld_dashboard_settings_data	 = $function_obj->ld_dashboard_settings_data();
$settings				 = $ld_dashboard_settings_data[ 'ld_dashboard_feed_settings' ];
$match_userroles = array();
if ( !empty($settings['disable_user_roles_live_feed']) ) {
	
	$match_userroles = array_intersect( $current_user_role, $settings['disable_user_roles_live_feed'] );
}

$class = "";
if ( ( isset($settings['disable-live-feed']) &&  $settings['disable-live-feed'] == 1 ) || ( !empty($match_userroles))){
	$class = 'ld-live-feed-hide';
}
?>
<div class="ld-dashboard-main-wrapper <?php echo esc_attr($class);?>">
	<?php

	include LD_DASHBOARD_PLUGIN_DIR . 'templates/ld-dashboard-profile.php';

	include LD_DASHBOARD_PLUGIN_DIR . 'templates/ld-dashboard-content-section.php';
	
	if ( ( !isset($settings['disable-live-feed']) ||  $settings['disable-live-feed'] != 1 ) && ( empty($match_userroles))): 
		include LD_DASHBOARD_PLUGIN_DIR . 'templates/ld-dashboard-feed-section.php';
	endif; ?>	
	
</div>