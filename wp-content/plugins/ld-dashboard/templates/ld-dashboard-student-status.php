<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $students,$course_ids, $wpdb;
$courseIDs = $course_ids;
$curr_user_id = get_current_user_id();
$group_ids = array();
if ( learndash_is_group_leader_user() ) {
	$group_student = learndash_get_group_leader_groups_users();
	
	$args = array(				
				'orderby'   => 'user_nicename',
				'order'     => 'ASC',
				'fields'    => array( 'ID', 'display_name' ),
				'include'	=> $group_student
			);
	
} else if ( learndash_is_admin_user()   ) {
	$args = array(
				//'meta_key'   => 'is_student',
				//'meta_value' => true,
				'orderby'    => 'user_nicename',
				'order'      => 'ASC',
				'fields'     => array( 'ID', 'display_name' ),
			);
} else {
	$instructor_students = $this->ld_dashboard_get_instructor_students_by_id( $curr_user_id );
	$course_student_ids = array(0);
	if ( ! empty( $instructor_students ) ) {
		$course_student_ids = array();
		foreach ( $instructor_students as $key => $course_student ) {
			$course_student_ids[] = $course_student->ID;
		}
	}
	$args = array(
				//'meta_key'   => 'is_student',
				//'meta_value' => true,
				'orderby'    => 'user_nicename',
				'order'      => 'ASC',
				'fields'     => array( 'ID', 'display_name' ),
				'include'	=> $course_student_ids
			);
			
	$sql_str = $wpdb->prepare("SELECT post_id,meta_key FROM ". $wpdb->postmeta ." as postmeta INNER JOIN ". $wpdb->posts ." as posts ON posts.ID=postmeta.post_id
				WHERE posts.post_type = %s AND posts.post_status = %s AND meta_key LIKE 'learndash_group_enrolled_%'", 'sfwd-courses', 'publish');	
	$group_courses = $wpdb->get_results( $sql_str );	
	if ( !empty($group_courses)) {
		foreach( $group_courses as $grp_course){
			$group_id = explode('learndash_group_enrolled_',$grp_course->meta_key);
			
			//$group_user_meta_key = 'learndash_group_users_'. $group_id[1];
			//$group_users = get_post_meta( $group_id[1], $group_user_meta_key, true);
			//$group_users = learndash_get_groups_user_ids( $group_id[1] );
			if ( in_array( $grp_course->post_id ,$courseIDs) && !in_array($group_id[1], $group_ids) ) {
				$group_ids[] = $group_id[1];
			}
		}
	}
}
$students     = get_users( $args );
if ( learndash_is_group_leader_user() && empty($group_student) ) {
	$students = array();
}

/* Check Student enrolled in any courses */
$isstudent = false;
if ( !empty($students)) { 
	foreach ( $students as $student ) { 
		$course_ids = learndash_user_get_enrolled_courses( $student->ID );
		if ( !empty($course_ids)) {
			$isstudent = true;
			break;
		}
	}
}


$loader  = includes_url( 'images/spinner-2x.gif' );
?>
<div class="ld-dashboard-student-status">
	<div class="ld-dashboard-seperator"><span><?php esc_html_e( 'Student Details', 'ld-dashboard' ); ?></span></div>

	<?php do_action( 'ld_dashboard_student_status_before', $curr_user_id ); ?>
	<?php if ( ! empty( $students ) && $isstudent == true ) { ?>
		<div class="ld-dashboard-student-status-block">
			<div class="ld-dashboard-student-lists">
				<?php if (!empty($group_ids)) :?>
					<select name="ld-dashboard-groups" class="ld-dashboard-groups" data-course-id="<?php echo join(',', $courseIDs);?>">
						<option value="all"><?php esc_html_e('All', 'ld-dashboard');?></option>
						
						<?php foreach( $group_ids as $grp_id ):?>
							<option value="<?php echo esc_attr($grp_id); ?>" ><?php echo get_the_title($grp_id); ?></option>
						<?php endforeach;?>
						
					</select>
				<?php endif;?>
				<select name="ld-dashboard-student" class="ld-dashboard-student">
					<?php foreach ( $students as $student ) { 
						$course_ids = learndash_user_get_enrolled_courses( $student->ID );
						if ( !empty($course_ids)) :?>
						<option value="<?php echo esc_attr($student->ID); ?>" ><?php echo esc_html($student->display_name); ?></option>
					<?php 
						endif;
					}?>
				</select>
			</div>
			<div class="ld-dashboard-student-loader">
				<img src="<?php echo apply_filters( 'ld_dashboard_loader_img_url', $loader ); ?>">
				<p><?php echo apply_filters( 'ld_dashboard_waiting_text', __( 'Please wait, while details are loading...', 'ld-dashboard' ) ); ?></p>
			</div>
			<div class="ld-dashboard-student-details"></div>
		</div>
	<?php } else { ?>
		<div class="ld-dashboard-student-status-no-students ld-dashboard-info">
			<p>
			<?php
			if ( in_array( 'ld_instructor', (array) $user->roles ) ) {
				echo apply_filters( 'ld_dashboard_student_status_no_student_instructor_message', __( 'Please make sure your course is enrolled by students.', 'ld-dashboard' ) );
			} else {
				echo apply_filters( 'ld_dashboard_student_status_no_student_admin_message', __( 'No registered student on the site.', 'ld-dashboard' ) );
			}
				?>
			</p>
		</div>
	<?php } ?>
	<?php do_action( 'ld_dashboard_student_status_after', $curr_user_id ); ?>
</div>
