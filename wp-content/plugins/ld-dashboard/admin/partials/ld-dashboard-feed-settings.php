<?php
/**
 * Provide a admin area view for the plugin.
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wbcomdesigns.com/plugins
 * @since      1.0.0
 *
 * @package    Ld_Dashboard
 * @subpackage Ld_Dashboard/admin/partials
 */
global $wp_roles;
$function_obj				 = Ld_Dashboard_Functions::instance();
$ld_dashboard_settings_data	 = $function_obj->ld_dashboard_settings_data();
$settings					 = $ld_dashboard_settings_data[ 'ld_dashboard_feed_settings' ];
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wbcom-tab-content">
	<div class="wrap ld-dashboard-settings">
		<div class="ld-dashboard-content container">
			<form method="post" action="options.php" enctype="multipart/form-data">
				<?php
				settings_fields( 'ld_dashboard_feed_settings' );
				do_settings_sections( 'ld_dashboard_feed_settings' );
				?>
				<div class="form-table">
					<div class="ld-grid-view-wrapper">						
						<div class="ld-single-grid">
							<div class="ld-grid-label" scope="row">
								<label><?php esc_html_e( 'Disable live feed? ', 'ld-dashboard' ); ?></label>
							</div>
							<div class="ld-grid-content">								
								<label class="ld-dashboard-setting-switch">
									<input type="checkbox" name="ld_dashboard_feed_settings[disable-live-feed]" value="1" <?php checked( $settings[ 'disable-live-feed' ], '1' ); ?> />
									<div class="ld-dashboard-setting round"></div>
								</label>
								<p class="ld-decription"><?php esc_html_e( 'Enable this option to hide live feed for all user.', 'ld-dashboard' ); ?></p>
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="form-table">
					<div class="ld-grid-view-wrapper">						
						<div class="ld-single-grid">
							<div class="ld-grid-label" scope="row">
								<label><?php esc_html_e( 'Disable user role wise live feed? ', 'ld-dashboard' ); ?></label>
							</div>
							<div class="ld-grid-content">
								<select id="wb_wss_seller_user_roles" name="ld_dashboard_feed_settings[disable_user_roles_live_feed][]" multiple>
									<?php
									$roles = $wp_roles->get_names();
									foreach( $roles as $role => $role_name ) {
										$selected = ( !empty( $settings['disable_user_roles_live_feed'] ) && in_array( $role, $settings['disable_user_roles_live_feed'] ) ) ? 'selected' : '';
									?>
									   <option value="<?php echo esc_attr( $role ); ?>" <?php echo $selected; ?>><?php echo esc_attr( $role_name ); ?></option>
									<?php } ?>
								</select>								
								<p class="description"><?php esc_html_e( 'Select user Roles to hide live feed for perticular user role wise.', 'ld-dashboard' ); ?></p>
							</div>
							
						</div>
					</div>
				</div>
				<?php submit_button(); ?>
				<?php wp_nonce_field( 'ld-dashboard-settings-submit', 'ld-dashboard-settings-submit' ); ?>
			</form>
		</div>
	</div>
</div>
