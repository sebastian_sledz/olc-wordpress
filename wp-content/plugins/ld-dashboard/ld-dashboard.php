<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wbcomdesigns.com/plugins
 * @since             1.0.0
 * @package           Ld_Dashboard
 *
 * @wordpress-plugin
 * Plugin Name:       Learndash Dashboard
 * Plugin URI:        https://github.com/vapvarun/ld-dashboard
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           4.1.0
 * Author:            Wbcom Designs
 * Author URI:        https://wbcomdesigns.com/plugins
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ld-dashboard
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'LD_DASHBOARD_VERSION', '4.1.0' );

define( 'LD_DASHBOARD_PLUGIN_DIR', plugin_dir_path(__FILE__) );
define( 'LD_DASHBOARD_PLUGIN_URL', plugins_url('/', __FILE__) );
if ( ! defined( 'LD_DASHBOARD_PLUGIN_FILE' ) ) {
	define( 'LD_DASHBOARD_PLUGIN_FILE', __FILE__ );
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ld-dashboard-activator.php
 */
function activate_ld_dashboard() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ld-dashboard-activator.php';
	Ld_Dashboard_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ld-dashboard-deactivator.php
 */
function deactivate_ld_dashboard() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ld-dashboard-deactivator.php';
	Ld_Dashboard_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ld_dashboard' );
register_deactivation_hook( __FILE__, 'deactivate_ld_dashboard' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ld-dashboard.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ld_dashboard() {

require plugin_dir_path(__FILE__) . 'edd-license/edd-plugin-license.php';
	$plugin = new Ld_Dashboard();
	$plugin->run();

}
//run_ld_dashboard();

/**
 * Include needed files if required plugin is active
 *  @since   1.0.0
 *  @author  Wbcom Designs
 */
add_action( 'plugins_loaded', 'ld_dashboard_plugin_init' );
function ld_dashboard_plugin_init() {
	if ( !in_array( 'sfwd-lms/sfwd_lms.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		add_action( 'admin_notices', 'ld_dashboard_admin_notice' );
	} else {
		run_ld_dashboard();
	}
}

/**
 * Show admin notice when Learndash not active or install.
 *  @since   1.0.0
 *  @author  Wbcom Designs
 */
function ld_dashboard_admin_notice() {
	?>
	<div class="error notice is-dismissible">
		<p><?php echo sprintf( __( 'The %s plugin requires %s plugin to be installed and active.', 'ld-dashboard' ), '<b>LearnDash Dashboard</b>', '<b>LearnDash</b>' ); ?></p>
	</div>
	<?php
}

add_action( 'admin_init' , 'ld_dashboard_update_admin_init' );
/*
 * Update To save wdm instructor id into ld instructor id.
 */
function ld_dashboard_update_admin_init() {
	global $wpdb, $pagenow;
	$update_ld_dashboard = get_option( 'update_ld_dashboard' );
	if ( !$update_ld_dashboard && ( $pagenow == 'plugins.php'  || ( isset($_GET['page']) && $_GET['page'] == 'ld-dashboard-settings' ) ) ) {
		
		ld_dashboard_update_wdm_instructor_to_ld_instructor();
		update_option( 'update_ld_dashboard', true );		
	}
}

function ld_dashboard_update_wdm_instructor_to_ld_instructor(){
	$args = array(
		'post_type'		 => 'sfwd-courses',
		'post_status'	 => 'publish',
		'posts_per_page' => -1,
		'meta_query'	=>array(									
							array(
								'key'     => 'ir_shared_instructor_ids',
								'value'   => '',
								'compare' => '!='
							)
						)
	);						
	$course = new WP_Query($args);	
	if ( $course->have_posts() ){

		while ( $course->have_posts() ) { $course->the_post();
			$_ld_instructor_ids = get_post_meta(get_the_ID(), '_ld_instructor_ids', true );
			if ( empty($_ld_instructor_ids)) {
				 $_ld_instructor_ids = array();
			}
			$ir_shared_instructor_ids = get_post_meta(get_the_ID(), 'ir_shared_instructor_ids', true );
			
			if ( $ir_shared_instructor_ids != '' ) {
				$ir_shared_instructor_ids = explode(',', $ir_shared_instructor_ids);
				
				foreach( $ir_shared_instructor_ids as $user_id ){
					$ld_user = new WP_User($user_id);
					$ld_user->add_role( 'ld_instructor' );
				}
			} else {
				$ir_shared_instructor_ids = array();
			}
			
			$_ld_instructor_ids = array_merge( $ir_shared_instructor_ids, $_ld_instructor_ids);
			update_post_meta(get_the_ID(), '_ld_instructor_ids', array_unique ($_ld_instructor_ids) );	
		}
		wp_reset_postdata();
	}
	
	$args = array(				
					'orderby'   => 'user_nicename',
					'role__in'	=> 'wdm_instructor',
					'order'     => 'ASC',
					'fields'    => array( 'ID', 'display_name' ),					
				);
					
	$instructors     = get_users( $args );
	if ( !empty($instructors)) {
		foreach ($instructors as $instructor){
			$ld_user = new WP_User($instructor->ID);
			$ld_user->add_role( 'ld_instructor' );
		}
	}
	
}
