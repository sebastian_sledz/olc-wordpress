#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gamipress LearnDash Integration\n"
"Report-Msgid-Bugs-To: https://gamipress.com/\n"
"POT-Creation-Date: 2021-01-28 10:22+0100\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: GamiPress (https://gamipress.com/)\n"
"Language-Team: GamiPress <contact@gamipress.com>\n"
"X-Generator: Poedit 2.4.2\n"
"X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c\n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ..\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: libraries\n"

#: gamipress-learndash.php:136
#, php-format
msgid "GamiPress - LearnDash integration requires %s and %s in order to work. Please install and activate them."
msgstr ""

#: includes/admin.php:24
msgid "LearnDash integration"
msgstr ""

#: includes/admin/recount-activity.php:23 includes/triggers.php:20
msgid "LearnDash"
msgstr ""

#: includes/admin/recount-activity.php:24
msgid "Recount quizzes completed"
msgstr ""

#: includes/admin/recount-activity.php:25
msgid "Recount topics completed"
msgstr ""

#: includes/admin/recount-activity.php:26
msgid "Recount lessons completed"
msgstr ""

#: includes/admin/recount-activity.php:27
msgid "Recount courses completed"
msgstr ""

#: includes/admin/recount-activity.php:61
#: includes/admin/recount-activity.php:62
#: includes/admin/recount-activity.php:160
#: includes/admin/recount-activity.php:161
#: includes/admin/recount-activity.php:251
#: includes/admin/recount-activity.php:252
#: includes/admin/recount-activity.php:335
#: includes/admin/recount-activity.php:336
#, php-format
msgid "%d users found, recounting..."
msgstr ""

#: includes/admin/recount-activity.php:105
#, php-format
msgid "[Complete quiz] Quiz: %s User: %s Course: %s Quiz Score: %s"
msgstr ""

#: includes/admin/recount-activity.php:112
#, php-format
msgid "[Complete quiz] Quiz: %s User: %s Quiz Score: %s"
msgstr ""

#: includes/admin/recount-activity.php:126
#: includes/admin/recount-activity.php:217
#: includes/admin/recount-activity.php:301
#: includes/admin/recount-activity.php:381
#, php-format
msgid "%d remaining users to finish recount"
msgstr ""

#: includes/admin/recount-activity.php:198
#, php-format
msgid "[Complete topic] Topic: %s User: %s Lesson: %s Course: %s"
msgstr ""

#: includes/admin/recount-activity.php:285
#, php-format
msgid "[Complete lesson] Lesson: %s User: %s Course: %s"
msgstr ""

#: includes/admin/recount-activity.php:367
#, php-format
msgid "[Complete course] Course: %s User: %s"
msgstr ""

#: includes/triggers.php:23
msgid "Complete a quiz"
msgstr ""

#: includes/triggers.php:24
msgid "Complete a specific quiz"
msgstr ""

#: includes/triggers.php:25
msgid "Complete any quiz of a specific course"
msgstr ""

#: includes/triggers.php:28
msgid "Complete a quiz with a minimum percent grade"
msgstr ""

#: includes/triggers.php:29
msgid "Complete a specific quiz with a minimum percent grade"
msgstr ""

#: includes/triggers.php:30
msgid "Complete a quiz of a specific course with a minimum percent grade"
msgstr ""

#: includes/triggers.php:33
msgid "Complete a quiz with a maximum percent grade"
msgstr ""

#: includes/triggers.php:34
msgid "Complete a specific quiz with a maximum percent grade"
msgstr ""

#: includes/triggers.php:35
msgid "Complete a quiz of a specific course with a maximum percent grade"
msgstr ""

#: includes/triggers.php:38
msgid "Complete a quiz on a range of percent grade"
msgstr ""

#: includes/triggers.php:39
msgid "Complete a specific quiz on a range of percent grade"
msgstr ""

#: includes/triggers.php:40
msgid "Complete a quiz of a specific course on a range of percent grade"
msgstr ""

#: includes/triggers.php:43
msgid "Successfully pass a quiz"
msgstr ""

#: includes/triggers.php:44
msgid "Successfully pass a specific quiz"
msgstr ""

#: includes/triggers.php:45
msgid "Successfully pass a quiz of a specific course"
msgstr ""

#: includes/triggers.php:48
msgid "Fail a quiz"
msgstr ""

#: includes/triggers.php:49
msgid "Fail a specific quiz"
msgstr ""

#: includes/triggers.php:50
msgid "Fail a quiz of a specific course"
msgstr ""

#: includes/triggers.php:53
msgid "Complete a topic"
msgstr ""

#: includes/triggers.php:54
msgid "Complete a specific topic"
msgstr ""

#: includes/triggers.php:55
msgid "Complete a topic of a specific course"
msgstr ""

#: includes/triggers.php:58
msgid "Upload an assignment"
msgstr ""

#: includes/triggers.php:59
msgid "Upload an assignment to a specific lesson"
msgstr ""

#: includes/triggers.php:60
msgid "Upload an assignment to a specific course"
msgstr ""

#: includes/triggers.php:61
msgid "Approve an assignment"
msgstr ""

#: includes/triggers.php:62
msgid "Approve an assignment of a specific lesson"
msgstr ""

#: includes/triggers.php:63
msgid "Approve an assignment of a specific course"
msgstr ""

#: includes/triggers.php:66
msgid "Complete a lesson"
msgstr ""

#: includes/triggers.php:67
msgid "Complete a specific lesson"
msgstr ""

#: includes/triggers.php:68
msgid "Complete a lesson of a specific course"
msgstr ""

#: includes/triggers.php:71
msgid "Enroll any course"
msgstr ""

#: includes/triggers.php:72
msgid "Enroll a specific course"
msgstr ""

#: includes/triggers.php:73
msgid "Complete a course"
msgstr ""

#: includes/triggers.php:74
msgid "Complete a specific course"
msgstr ""

#: includes/triggers.php:77
msgid "Join any group"
msgstr ""

#: includes/triggers.php:78
msgid "Join a specific group"
msgstr ""

#: includes/triggers.php:160
#, php-format
msgid "Completed a quiz with a score of %d or higher"
msgstr ""

#: includes/triggers.php:164
#, php-format
msgid "Complete the quiz %s with a score of %d or higher"
msgstr ""

#: includes/triggers.php:168
#, php-format
msgid "Complete a quiz of the course %s with a score of %d or higher"
msgstr ""

#: includes/triggers.php:173
#, php-format
msgid "Completed a quiz with a maximum score of %d"
msgstr ""

#: includes/triggers.php:177
#, php-format
msgid "Complete the quiz %s with a maximum score of %d"
msgstr ""

#: includes/triggers.php:181
#, php-format
msgid "Complete a quiz of the course %s with a maximum score of %d"
msgstr ""

#: includes/triggers.php:186
#, php-format
msgid "Completed a quiz with a score between %d and %d"
msgstr ""

#: includes/triggers.php:190
#, php-format
msgid "Complete the quiz %s with a score between %d and %d"
msgstr ""

#: includes/triggers.php:194
#, php-format
msgid "Complete a quiz of the course %s with a score between %d and %d"
msgstr ""

#: includes/triggers.php:214
#, php-format
msgid "Complete the quiz %s"
msgstr ""

#: includes/triggers.php:215
#, php-format
msgid "Complete any quiz of the course %s"
msgstr ""

#: includes/triggers.php:216
#, php-format
msgid "Pass the quiz %s"
msgstr ""

#: includes/triggers.php:217
#, php-format
msgid "Pass a quiz of the course %s"
msgstr ""

#: includes/triggers.php:218
#, php-format
msgid "Fail the quiz %s"
msgstr ""

#: includes/triggers.php:219
#, php-format
msgid "Fail a quiz of the course %s"
msgstr ""

#: includes/triggers.php:222
#, php-format
msgid "Complete the topic %s"
msgstr ""

#: includes/triggers.php:223
#, php-format
msgid "Complete a topic of the course %s"
msgstr ""

#: includes/triggers.php:226
#, php-format
msgid "Upload an assignment to the lesson %s"
msgstr ""

#: includes/triggers.php:227
#, php-format
msgid "Upload an assignment to the course %s"
msgstr ""

#: includes/triggers.php:228
#, php-format
msgid "Approve an assignment of the lesson %s"
msgstr ""

#: includes/triggers.php:229
#, php-format
msgid "Approve an assignment of the course %s"
msgstr ""

#: includes/triggers.php:232
#, php-format
msgid "Complete the lesson %s"
msgstr ""

#: includes/triggers.php:233
#, php-format
msgid "Complete a lesson of the course %s"
msgstr ""

#: includes/triggers.php:236
#, php-format
msgid "Enroll in course %s"
msgstr ""

#: includes/triggers.php:237
#, php-format
msgid "Complete the course %s"
msgstr ""

#: includes/triggers.php:240
#, php-format
msgid "Join %s group"
msgstr ""

#: includes/triggers.php:497
msgid "Grade of completion"
msgstr ""

#: includes/triggers.php:498
msgid "Grade of completion the user got on complete this quiz."
msgstr ""
