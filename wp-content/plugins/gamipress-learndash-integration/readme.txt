=== GamiPress - LearnDash integration ===
Contributors: gamipress, tsunoa, rubengc, eneribs
Tags: learndash, lms, learning, gamipress, gamification, points, achievements, badges, awards, rewards, credits, engagement, elearning, Learning Management System, courses, lessons, topics, quiz, training, guru, sell coursesRequires at least: 4.4
Tested up to: 5.7
Stable tag: 1.2.7
License: GNU AGPLv3
License URI: http://www.gnu.org/licenses/agpl-3.0.html

Connect GamiPress with LearnDash

== Description ==

Gamify your [LearnDash](http://www.learndash.com "LearnDash") LMS site thanks to the powerful gamification plugin, [GamiPress](https://wordpress.org/plugins/gamipress/ "GamiPress")!

This plugin automatically connects GamiPress with LearnDash adding new activity events.

= New Events =

= For complete a quiz =

* Complete any quiz: When a user finish a quiz
* Complete a specific quiz: When a user finish a specific quiz
* Complete any quiz of a specific course: When a user finish a quiz of a specific course

= For complete a quiz at minimum grade =

* Complete any quiz with a minimum percent grade: When a user finish a quiz with a minimum percent grade
* Complete a specific quiz with a minimum percent grade: When a user finish a specific quiz with a minimum percent grade
* Complete a quiz of a specific course with a minimum percent grade: When a user finish a quiz of a specific course with a minimum percent grade

= For complete a quiz at maximum grade =

* Complete any quiz with a maximum percent grade: When a user finish a quiz with a maximum percent grade
* Complete a specific quiz with a maximum percent grade: When a user finish a specific quiz with a maximum percent grade
* Complete a quiz of a specific course with a maximum percent grade: When a user finish a quiz of a specific course with a maximum percent grade

= For complete a quiz at between a range of grades =

* Complete any quiz on a range of percent grade: When a user finish a quiz on a range of percent grade
* Complete a specific quiz on a range of percent grade: When a user finish a specific quiz on a range of percent grade
* Complete a quiz of a specific course on a range of percent grade: When a user finish a quiz of a specific course on a range of percent grade

= For pass a quiz =

* Pass any quiz: When a user successfully pass a quiz
* Pass a specific quiz: When a user successfully pass a specific quiz
* Pass a quiz of a specific course: When a user successfully pass a quiz of a specific course

= For fail a quiz =

* Fail any quiz: When a user fails a quiz
* Fail a specific quiz: When a user fails a specific quiz
* Fail a quiz of a specific course: When a user fails a quiz of a specific course

= For Topics =

* Complete any topic: When a user finish a topic
* Complete a specific topic: When a user finish a specific topic
* Complete any topic of a specific course: When a user finish a topic of a specific course

= For Assignments =

* Upload an assignment: When a user uploads an assignment
* Upload an assignment to a specific lesson: When a user uploads an assignment to a specific lesson
* Upload an assignment to a specific course: When a user uploads an assignment to a specific course
* Approve an assignment: When a user gets approved an assignment
* Approve an assignment of a specific lesson: When a user gets approved an assignment of a specific lesson
* Approve an assignment of a specific course: When a user gets approved an assignment of a specific course

= For Lessons =

* Complete any lesson: When a user complete a lesson
* Complete a specific lesson: When a user complete a specific lesson
* Complete any lesson of a specific course: When a user complete a lesson of a specific course

= For Courses =

* Enroll in any course: When a user gets enrolled in a course
* Enroll in a specific course: When a user gets enrolled in a specific course
* Complete any course: When a user finish a course
* Complete a specific course: When a user finish a specific course

= For Groups =

* Join any group: When a user joins a group.
* Join a specific group: When a user joins a specific group.

== Installation ==

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.2.7 =

* **New Features**
* New event: Join any group.
* New event: Join a specific group.

= 1.2.6 =

* **New Features**
* New event: Enroll in any course.
* New event: Enroll in a specific course.

= 1.2.5 =

* **Improvements**
* Performance improvements by limiting the number of requirements to check to only those who match with the event parameters.

= 1.2.4 =

* **Improvements**
* Added "Grade of completion" field on logs related with events that has grade checks.
* Added support to latest GamiPress update to provide log query field type.

= 1.2.3 =

* **Bug Fixes**
* Fixed incorrect log count for grade-related events.

= 1.2.2 =

* **Improvements**
* Make sure to don't recount elements that are not correctly setup or has missed data.

= 1.2.1 =

* **Bug Fixes**
* Prevent issues on Recount Activity tool caused by quizzes without a course assigned.

= 1.2.0 =

* **Improvements**
* Improvements on the recount activity processes.
* Added support to the brand new recount activity log (that will be released on GamiPress 1.8.2) to provide information about what happens on a recount activity process.

= 1.1.9 =

* **Bug Fixes**
* Avoid to exceed GamiPress awards engine limits on the Recount Activity Tool.

= 1.1.8 =

* **Bug Fixes**
* Fixed internal server error on quiz completion recount at the Recount Activity Tool.

= 1.1.7 =

* **Development Notes**
* Split quiz events to allow unbind them for performance improvements.

= 1.1.6 =

* **New Features**
* Added new activity events related to complete a quiz between a range of scores.
* **Improvements**
* Removed some backward compatibility checks.

= 1.1.5 =

* **New Features**
* Added support to GamiPress 1.4.8 multisite features.

= 1.1.4 =

* **Improvements**
* Improvements on the awards engine checks to ensure access to given requirement.

= 1.1.3 =

* **New Features**
* Added new activity events related to assignments upload and approval.

= 1.1.2 =

* **Bug Fixes**
* Fixed quizzes events that are not being award.
* Fixed wrong offset on recount activity tool.

= 1.1.1 =

* **New Features**
* Added quizzes activity events based on a specific course.
* Added topics activity events based on a specific course.
* Added lessons activity events based on a specific course.

= 1.1.0 =

* **Improvements**
* Improvements and bug fixes on recount activity tool.
* Moved old changelog to changelog.txt file.
