# README #

The repository to develop and maintain LearnDash plugin REST API extension with new custom endpoints.

### Starting point ###

Test OLC WordPress instance:
[https://olc.freeformcode.com/](https://olc.freeformcode.com/)

Login/signup page:
[https://olc.freeformcode.com/login/](https://olc.freeformcode.com/login/)

Base course to fetch:
[https://olc.freeformcode.com/courses/performance-mindset-5-week/](https://olc.freeformcode.com/courses/performance-mindset-5-week/)

LearnDash (our LMS) API documentation:
[https://developers.learndash.com/](https://developers.learndash.com/)

### How do I get set up? ###

- Clone repository and commit changes to test on instance.
- Each push to master branch will run a build and deploy LearnDash plugin files to its directory in WordPress: ../wp-content/plugins/sfwd-lms
- LearnDash API files are located in ../includes/rest-api
- Quick, small changes can be tested directly via WordPress dashboard in Plugins > Plugin Editor > select plugin > choose file to edit from 
- Test instance is using Basic Auth to authenticate API calls.
- Use issue tracker to collaborate on specific topics.
- Use Wiki to describe existing/new endpoints that are/will be in use by the PP app.

**Admin credentials that can be used in Postman to test calls:**

`PP_ad_min / @q!f9EH$P%6MD%WbkSJ5AUo%`

---

Version 0.1